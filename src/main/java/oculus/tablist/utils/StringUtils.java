package oculus.tablist.utils;

import java.util.Objects;

public class StringUtils {
    public static String join(CharSequence delimiter, Iterable<? extends CharSequence> elements) {
        Objects.requireNonNull(delimiter);
        Objects.requireNonNull(elements);

        String new_string = null;
        for (CharSequence cs : elements) {
            if (new_string == null) {
                new_string = (String) cs;
            } else {
                new_string += (String) delimiter + cs;
            }
        }

        return new_string;
    }

    public static String join(CharSequence delimiter, CharSequence... elements) {
        Objects.requireNonNull(delimiter);
        Objects.requireNonNull(elements);

        String new_string = null;
        for (CharSequence cs : elements) {
            if (new_string == null) {
                new_string = (String) cs;
            } else {
                new_string += (String) delimiter + cs;
            }
        }
        return new_string;
    }
}
